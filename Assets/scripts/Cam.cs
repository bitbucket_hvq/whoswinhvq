using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam : MonoBehaviour
{
    public static Cam Cam_instance;


    public List<Transform> PosTimeline = new List<Transform>();
    public void Start()
    {
        if (Cam_instance == null)
        {
            Cam_instance = this;
        }
    }

    public void NextToPosTimeLine(int numberTimeline)
    {
        gameObject.transform.position = PosTimeline[numberTimeline].transform.position;
    }
}
