using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action : MonoBehaviour
{
    public static Action AC_instance;

    private int numPlayerWin;
    private int numPlayerValue;

    public void Start()
    {
        if(AC_instance != null){
            Destroy(this);
        }

        AC_instance = this;
    }
    

    public void SetValuePlayer(int value)
    {
        numPlayerValue = value;
        GameControl.GC_instance.EnableButton();
        Debug.Log("settingPlayerValue " + value);
    }
    public void SetPlayerWin(int value)
    {
        numPlayerWin = value;   
    }

    public bool KetQua()
    {       
        if (numPlayerWin == numPlayerValue)
        {
            numPlayerValue = 0;
            return true;
        }
        numPlayerValue = 0;
        return false;
    }
}
