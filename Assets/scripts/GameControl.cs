using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using System;

public class GameControl : MonoBehaviour
{
    public static GameControl GC_instance;
    //=============UI=====================
    public Canvas cansNext;
    public GameObject point;




    //==============LIST==================
    //timeline hien tai load
    public int numTl;


    //list timeline
    public List<PlayableDirector> playableDirectorOne
        = new List<PlayableDirector>();

    public List<PlayableDirector> playableDirectorTwo
        = new List<PlayableDirector>();

    //list player duoc chon se WIN game 
    //ONLY player 1 hoac 2
    public List<int> playerWin = new List<int>();

    // 1-2


    //=========================================
    


    public void Start()
    {
        if (GC_instance != null)
            Destroy(this);
        GC_instance = this;
        
        RunStoryOne();

    }


    //click
    public void Update()
    {
        Vector3 mousePos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Instantiate(point, mousePos, Quaternion.identity);
        
    }
 

    private void RunStoryOne()
    {
        cansNext.gameObject.SetActive(false);
        playableDirectorOne[numTl].Play();
        Cam.Cam_instance.NextToPosTimeLine(numTl);
        Action.AC_instance.SetPlayerWin(playerWin[numTl]);
    }

    private void RunStoryTwo()
    {
        cansNext.gameObject.SetActive(false);
        playableDirectorOne[numTl].Play();
        Action.AC_instance.SetPlayerWin(playerWin[numTl]);
    }

    // hien button len
    public void EnableButton()
    {
        cansNext.gameObject.SetActive(true);
    }

    //khi click Button Next den Story 2
    public void ClickButtonNext(int player)
    {
        bool ketqua = Action.AC_instance.KetQua();
        Debug.Log("ket qua : " + ketqua);

        RunStoryTwo();

        numTl++;
        RunStoryOne();
    }
}



